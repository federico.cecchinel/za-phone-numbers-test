const _ = require('lodash');
const path = require('path');
const fs = require('fs');
const DBConnection = require('../src/database/connection');
const dotenv = require('dotenv');
const request = require('supertest');
const { randomString } = require('./utils');
const { API_PHONE_NUMBER_TEST, API_PHONE_NUMBER_TEST_CSV_FILE, API_PHONE_NUMBER_FIND, KEY_ID, KEY_SMS_PHONE } = require('../globals');

dotenv.config({ path: path.join(process.cwd(), '.env.test') });

const app = require('../app');

describe('Server api test', function () {
    const agent = request.agent(app);

    before('Connect to database', async function () {
        await DBConnection.connect(process.env.DATABASE);
        return await DBConnection.clearDatabase();
    });

    /*after('Cleaning database', async function () {
        return await DBConnection.clearDatabase();
    });*/

    describe('#test single phone number, valid number', function () {
        it('test valid phone number 27736529279, no correction needed', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '27736529279';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) { throw new Error('Invalid response'); }
                    if (response.success !== true) { throw new Error(response.error || 'Response failed without reason'); }
                    if (response.original_value !== phoneNumber) { throw new Error('Response \'original_value\' property mismatched with supplied phone number value'); }
                    if (response.is_valid === false || response.corrected === true) { throw new Error('Testing on phone number failed'); }
                    done();
                });
        });
    });

    describe('#test single phone number, valid number after corrections', function () {
        it('test valid phone number 736529279, missing prefix', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '736529279';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) { throw new Error('Invalid response'); }
                    if (response.success !== true) { throw new Error(response.error || 'Response failed without reason'); }
                    if (response.original_value !== phoneNumber) { throw new Error('Response \'original_value\' property mismatched with supplied phone number value'); }
                    if (response.is_valid === false || response.corrected === false) { throw new Error('Testing on phone number failed'); }
                    if (!_.isPlainObject(response.corrections)) { throw new Error('Missing \'correction\' property'); }
                    const correctionKeys = Reflect.ownKeys(response.corrections);
                    if (correctionKeys.length > 1 || !_.includes(correctionKeys, 'addedPrefix')) { throw new Error('Response \'corrections\' object contains invalid properties'); }
                    done();
                });
        });


        it('test valid phone number 26736529279, invalid prefix \'26\'', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '26736529279';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (response.original_value !== phoneNumber) throw new Error('Response \'original_value\' property mismatched with supplied phone number value');
                    if (response.is_valid === false || response.corrected === false) throw new Error('Testing on phone number failed');
                    if (!_.isPlainObject(response.corrections)) throw new Error('Missing \'correction\' property');
                    const correctionKeys = Reflect.ownKeys(response.corrections);
                    if (correctionKeys.length > 2 || !_.includes(correctionKeys, 'changedPrefix') || !_.includes(correctionKeys, 'oldPrefix') || response.corrections.oldPrefix !== '26')
                        throw new Error('Response \'corrections\' object contains invalid properties');
                    done();
                });
        });

        it('test valid phone number 277365292793, extra digit \'3\' at the end', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '277365292793';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (response.original_value !== phoneNumber) throw new Error('Response \'original_value\' property mismatched with supplied phone number value');
                    if (response.is_valid === false || response.corrected === false) throw new Error('Testing on phone number failed');
                    if (!_.isPlainObject(response.corrections)) throw new Error('Missing \'correction\' property');
                    const correctionKeys = Reflect.ownKeys(response.corrections);
                    if (correctionKeys.length > 1 || !_.includes(correctionKeys, 'deleteEnd') || response.corrections.deleteEnd !== '3')
                        throw new Error('Response \'corrections\' object contains invalid properties');
                    done();
                });
        });

        it('test valid phone number 7365292793, no prefix and extra digit \'3\' at start the end', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '7365292793';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (response.original_value !== phoneNumber) throw new Error('Response \'original_value\' property mismatched with supplied phone number value');
                    if (response.is_valid === false || response.corrected === false) throw new Error('Testing on phone number failed');
                    if (!_.isPlainObject(response.corrections)) throw new Error('Missing \'correction\' property');
                    const correctionKeys = Reflect.ownKeys(response.corrections);
                    if (correctionKeys.length > 2 || !_.includes(correctionKeys, 'addedPrefix') || !_.includes(correctionKeys, 'deleteEnd') || response.corrections.deleteEnd !== '3')
                        throw new Error('Response \'corrections\' object contains invalid properties');
                    done();
                });
        });

        it('test valid phone number 7365292793, no prefix and extra digit \'3\' at the end', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '7365292793';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (response.original_value !== phoneNumber) throw new Error('Response \'original_value\' property mismatched with supplied phone number value');
                    if (response.is_valid === false || response.corrected === false) throw new Error('Testing on phone number failed');
                    if (!_.isPlainObject(response.corrections)) throw new Error('Missing \'correction\' property');
                    const correctionKeys = Reflect.ownKeys(response.corrections);
                    if (correctionKeys.length > 2 || !_.includes(correctionKeys, 'addedPrefix') || !_.includes(correctionKeys, 'deleteEnd') || response.corrections.deleteEnd !== '3')
                        throw new Error('Response \'corrections\' object contains invalid properties');
                    done();
                });
        });

        it('test valid phone number 263716791426, wrong prefix and extra digit \'3\' at start', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '263716791426';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (response.original_value !== phoneNumber) throw new Error('Response \'original_value\' property mismatched with supplied phone number value');
                    if (response.is_valid === false || response.corrected === false) throw new Error('Testing on phone number failed');
                    if (!_.isPlainObject(response.corrections)) throw new Error('Missing \'correction\' property');
                    const correctionKeys = Reflect.ownKeys(response.corrections);
                    if (correctionKeys.length > 3 || !_.includes(correctionKeys, 'changedPrefix') || !_.includes(correctionKeys, 'oldPrefix') || !_.includes(correctionKeys, 'deleteStart') || response.corrections.deleteStart !== '3' || response.corrections.oldPrefix !== '26')
                        throw new Error('Response \'corrections\' object contains invalid properties');
                    done();
                });
        });

        it('test valid phone number _DELETED_27736529279, extra text \'_DELETED_\' at start', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '_DELETED_27736529279';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (response.original_value !== phoneNumber) throw new Error('Response \'original_value\' property mismatched with supplied phone number value');
                    if (response.is_valid === false || response.corrected === false) throw new Error('Testing on phone number failed');
                    if (!_.isPlainObject(response.corrections)) throw new Error('Missing \'correction\' property');
                    const correctionKeys = Reflect.ownKeys(response.corrections);
                    if (correctionKeys.length > 1 || !_.includes(correctionKeys, 'deleteStart') || response.corrections.deleteStart !== '_DELETED_')
                        throw new Error('Response \'corrections\' object contains invalid properties');
                    done();
                });
        });

        it('test valid phone number _DELETED_736529279, extra text \'_DELETED_\' at start and missing prefix', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '_DELETED_736529279';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (response.original_value !== phoneNumber) throw new Error('Response \'original_value\' property mismatched with supplied phone number value');
                    if (response.is_valid === false || response.corrected === false) throw new Error('Testing on phone number failed');
                    if (!_.isPlainObject(response.corrections)) throw new Error('Missing \'correction\' property');
                    const correctionKeys = Reflect.ownKeys(response.corrections);
                    if (correctionKeys.length > 2 || !_.includes(correctionKeys, 'deleteStart') || !_.includes(correctionKeys, 'addedPrefix') || response.corrections.deleteStart !== '_DELETED_')
                        throw new Error('Response \'corrections\' object contains invalid properties');
                    done();
                });
        });

        it('test valid phone number _DELETED_7365292793, extra text \'_DELETED_\' at start, extra character \'3\' at the end, and missing prefix', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '_DELETED_7365292793';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (response.original_value !== phoneNumber) throw new Error('Response \'original_value\' property mismatched with supplied phone number value');
                    if (response.is_valid === false || response.corrected === false) throw new Error('Testing on phone number failed');
                    if (!_.isPlainObject(response.corrections)) throw new Error('Missing \'correction\' property');
                    const correctionKeys = Reflect.ownKeys(response.corrections);
                    if (correctionKeys.length > 3 || !_.includes(correctionKeys, 'deleteStart') || !_.includes(correctionKeys, 'deleteEnd') || !_.includes(correctionKeys, 'addedPrefix') || response.corrections.deleteStart !== '_DELETED_' || response.corrections.deleteEnd !== '3')
                        throw new Error('Response \'corrections\' object contains invalid properties');
                    done();
                });
        });
    });

    describe('#test single phone number, invalid values', function () {
        it('test invalid value 639156553262', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '639156553262';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (response.original_value !== phoneNumber) throw new Error('Response \'original_value\' property mismatched with supplied phone number value');
                    if (response.is_valid !== false) throw new Error('Testing on phone number failed');
                    done();
                });
        });

        it('test invalid value 639156553262_DELETED', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '639156553262_DELETED';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (response.original_value !== phoneNumber) throw new Error('Response \'original_value\' property mismatched with supplied phone number value');
                    if (response.is_valid !== false) throw new Error('Testing on phone number failed');
                    done();
                });
        });
        it('test invalid value 262_DELETED_27717217884', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '262_DELETED_27717217884';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (response.original_value !== phoneNumber) throw new Error('Response \'original_value\' property mismatched with supplied phone number value');
                    if (response.is_valid !== false) throw new Error('Testing on phone number failed');
                    done();
                });
        });
        it('test invalid value PROVA123', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = 'PROVA123';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (response.original_value !== phoneNumber) throw new Error('Response \'original_value\' property mismatched with supplied phone number value');
                    if (response.is_valid !== false) throw new Error('Testing on phone number failed');
                    done();
                });
        });
        it('test with empty string', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== false) throw new Error(response.error || 'Response not failed for unknown reason');
                    done();
                });
        });

        it('test with empty id', function (done) {
            const id = '';
            const phoneNumber = '27717217884';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== false) throw new Error(response.error || 'Response not failed for unknown reason');
                    done();
                });
        });

        it('test with empty phone number', function (done) {
            const id = randomString(10, '0123456789');
            const phoneNumber = '';

            agent
                .post(API_PHONE_NUMBER_TEST)
                .send({ [KEY_ID]: id, [KEY_SMS_PHONE]: phoneNumber })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== false) throw new Error(response.error || 'Response not failed for unknown reason');
                    done();
                });
        });
    });

    /*
     Successful: 459
     Corrected: 190
     Failed: 351*/


     describe('#test multi phone number from csv', function () {
        it('test csv \'phone_numbers.csv\'', function (done) {
            const filename = 'phone_numbers.csv';
            const file = fs.readFileSync(path.join(process.cwd(), 'test', filename));

            const expectedItemsLength = 1000;
            const expectedSuccessful = 459;
            const expectedCorrected = 190;
            const expectedFailed = 351;

            agent
                .post(API_PHONE_NUMBER_TEST_CSV_FILE)
                .attach('file', file, { filename, contentType: 'text/csv' })
                .timeout({
                    response: 10000,  // Wait 10 seconds for the server to start sending,
                })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (!_.has(response, 'items') || !_.isArray(response.items)) throw new Error('Missing or invalid items list');
                    if (!_.has(response, 'stats') || !_.isPlainObject(response.stats)) throw new Error('Missing or invalid \'statistics\' object');

                    if (response.stats.successful !== expectedSuccessful) throw new Error(`Expected successful is ${expectedSuccessful}, but got ${response.stats.successful}`);
                    if (response.stats.corrected !== expectedCorrected) throw new Error(`Expected corrected is ${expectedCorrected}, but got ${response.stats.corrected}`);
                    if (response.stats.failure !== expectedFailed) throw new Error(`Expected failure is ${expectedFailed}, but got ${response.stats.failure}`);

                    if (response.items.length !== expectedItemsLength) throw new Error(`Items list length mismatching, expected ${expectedItemsLength}, got ${response.items.length}`)

                    done();
                });
        });
    });

    describe('#find phone numbers', function () {
        it('find all numbers', function (done) {
            const expectedItemsLength = 1014;
            const expectedSuccessful = 460;
            const expectedCorrected = 199;
            const expectedFailed = 355;

            agent
                .get(API_PHONE_NUMBER_FIND)
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (!_.has(response, 'items') || !_.isArray(response.items)) throw new Error('Missing or invalid items list');
                    if (!_.has(response, 'stats') || !_.isPlainObject(response.stats)) throw new Error('Missing or invalid \'statistics\' object');

                    if (response.stats.successful !== expectedSuccessful) throw new Error(`Expected successful is ${expectedSuccessful}, but got ${response.stats.successful}`);
                    if (response.stats.corrected !== expectedCorrected) throw new Error(`Expected corrected is ${expectedCorrected}, but got ${response.stats.corrected}`);
                    if (response.stats.failure !== expectedFailed) throw new Error(`Expected failure is ${expectedFailed}, but got ${response.stats.failure}`);

                    if (response.items.length !== expectedItemsLength) throw new Error(`Items list length mismatching, expected ${expectedItemsLength}, got ${response.items.length}`)

                    done();
                });
        });

        it('search by id 103225830', function (done) {
            const expectedItemsLength = 1;
            const expectedSuccessful = 0;
            const expectedCorrected = 0;
            const expectedFailed = 1;

            const search = "103225830"

            agent
                .get(API_PHONE_NUMBER_FIND)
                .query({"search": search})
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (!_.has(response, 'items') || !_.isArray(response.items)) throw new Error('Missing or invalid items list');
                    if (!_.has(response, 'stats') || !_.isPlainObject(response.stats)) throw new Error('Missing or invalid \'statistics\' object');

                    if (response.stats.successful !== expectedSuccessful) throw new Error(`Expected successful is ${expectedSuccessful}, but got ${response.stats.successful}`);
                    if (response.stats.corrected !== expectedCorrected) throw new Error(`Expected corrected is ${expectedCorrected}, but got ${response.stats.corrected}`);
                    if (response.stats.failure !== expectedFailed) throw new Error(`Expected failure is ${expectedFailed}, but got ${response.stats.failure}`);

                    if (response.items.length !== expectedItemsLength) throw new Error(`Items list length mismatching, expected ${expectedItemsLength}, got ${response.items.length}`)

                    done();
                });
        });

        it('search by original value _DELETED_1488176172', function (done) {
            const expectedItemsLength = 1;
            const expectedSuccessful = 0;
            const expectedCorrected = 0;
            const expectedFailed = 1;

            const search = "_DELETED_1488176172"

            agent
                .get(API_PHONE_NUMBER_FIND)
                .query({"search": search})
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (!_.has(response, 'items') || !_.isArray(response.items)) throw new Error('Missing or invalid items list');
                    if (!_.has(response, 'stats') || !_.isPlainObject(response.stats)) throw new Error('Missing or invalid \'statistics\' object');

                    if (response.stats.successful !== expectedSuccessful) throw new Error(`Expected successful is ${expectedSuccessful}, but got ${response.stats.successful}`);
                    if (response.stats.corrected !== expectedCorrected) throw new Error(`Expected corrected is ${expectedCorrected}, but got ${response.stats.corrected}`);
                    if (response.stats.failure !== expectedFailed) throw new Error(`Expected failure is ${expectedFailed}, but got ${response.stats.failure}`);

                    if (response.items.length !== expectedItemsLength) throw new Error(`Items list length mismatching, expected ${expectedItemsLength}, got ${response.items.length}`)

                    done();
                });
        });

        it('search by phone number found 27746041495', function (done) {
            const expectedItemsLength = 1;
            const expectedSuccessful = 1;
            const expectedCorrected = 0;
            const expectedFailed = 0;

            const search = "27746041495"

            agent
                .get(API_PHONE_NUMBER_FIND)
                .query({"search": search})
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) throw err;

                    const response = res.body;

                    if (!_.isPlainObject(response)) throw new Error('Invalid response');
                    if (response.success !== true) throw new Error(response.error || 'Response failed without reason');
                    if (!_.has(response, 'items') || !_.isArray(response.items)) throw new Error('Missing or invalid items list');
                    if (!_.has(response, 'stats') || !_.isPlainObject(response.stats)) throw new Error('Missing or invalid \'statistics\' object');

                    if (response.stats.successful !== expectedSuccessful) throw new Error(`Expected successful is ${expectedSuccessful}, but got ${response.stats.successful}`);
                    if (response.stats.corrected !== expectedCorrected) throw new Error(`Expected corrected is ${expectedCorrected}, but got ${response.stats.corrected}`);
                    if (response.stats.failure !== expectedFailed) throw new Error(`Expected failure is ${expectedFailed}, but got ${response.stats.failure}`);

                    if (response.items.length !== expectedItemsLength) throw new Error(`Items list length mismatching, expected ${expectedItemsLength}, got ${response.items.length}`)

                    done();
                });
        });
    });
})