const _ = require('lodash');
const NotAMobilePhoneError = require('./errors/not-a-mobile-phone-error');
const InvalidPhoneError = require('./errors/invalid-phone-number-error');
const { parsePhoneNumber, PhoneNumber } = require('libphonenumber-js/max');

const DEFAULT_COUNTRY_CODE = 'ZA';
const DEFAULT_COUNTRY_CALLING_CODE = '27';
const STANDARD_PHONE_NUMBER_LENGTH = 11;   // including 27 internation prefix at start

/**
 * 
 * @typedef {object} PhoneNumberTestResponse
 * @property {string} originalValue
 * @property {string|null} [correctValue]
 * @property {boolean} [isValid]
 * @property {boolean} [isPossible]
 * @property {boolean} [corrected]
 * @property {PhoneNumberCorrections} [corrections]
 */

/**
 * 
 * @typedef {object} PhoneNumberCorrections
 * @property {boolean} [addedPrefix]
 * @property {boolean} [changedPrefix]
 * @property {string} [oldPrefix]
 * @property {string} [deleteStart]
 * @property {string} [deleteEnd]
 */

/**
 * Verifica se il testo passato contiene un numero di telefono, 
 * e se valido verifica che sia di tipo MOBILE.
 * Viene controllato poi le modifiche apportate per ottenere il
 * numero di telefono corretto
 * 
 * @param {string} text 
 * @throws {NotAMobilePhoneError|InvalidPhoneError|ParserError|TypeError|Error}
 * @returns {PhoneNumberTestResponse}
 */
exports.test = function (text) {
    if (typeof text !== 'string') {
        throw new TypeError('Value supplied is not a string');
    }

    if (text.length === 0) {
        throw new Error('Empty value supplied');
    }

    /**
     * @type {PhoneNumberTestResponse}
     */
    const response = {
        originalValue: text,
        correctValue: null,
        isValid: false,
        isPossible: false
    }

    const phoneNumber = parsePhoneNumber(text, { defaultCountry: DEFAULT_COUNTRY_CODE });

    if (phoneNumber) {
        if (phoneNumber.isValid() === true) {
            const phoneNumberType = phoneNumber.getType();
            if (phoneNumberType !== 'MOBILE') {
                throw new NotAMobilePhoneError('Not a mobile number');
            }
        }

        if (phoneNumber.isPossible() === false) {
            throw new InvalidPhoneError('Not a valid phone number');
        }

        response.correctValue = phoneNumber.number;
        response.isValid = phoneNumber.isValid();
        response.isPossible = phoneNumber.isPossible();

        // eseguo un substring(1) sul numero di telefono per togliere il + iniziale
        response.corrections = _checkNumberChanges(text, phoneNumber.number.substr(1));
        if (!_.isEmpty(response.corrections)) {
            response.corrected = true;
        }

        if (response.isPossible === false && response.correctValue.substr(1).length > STANDARD_PHONE_NUMBER_LENGTH) {
            throw new InvalidPhoneError('Not a valid phone number');
        }

        return response;
    }

    throw new InvalidPhoneError('Not a valid phone number');
}

/**
 * Verifica e cerca di risolvere eventuali errori del numero di telefono
 * Errori comuni sono:
 * - prefisso sbagliato
 * - cifra in più alla fine
 * - cifra in più all'inizio
 * Il numero viene ritenuto corretto quando, con mnnpcxrefisso internazionale,
 * risulta essere lungo esattamente 11 caratteri (es: 27 83 1234567‬)
 * 
 * @param {string} text 
 * @throws {NotAMobilePhoneError|InvalidPhoneError|ParserError|TypeError|Error}
 * @return {PhoneNumberTestResponse}
 */
exports.testAndResolve = function (text) {
    const originalText = text;

    /**
     * @type {PhoneNumberTestResponse}
     */
    let response = null;

    try {
        response = exports.test(text);

        if (response.isValid) {
            return response;
        }
    } catch (e) {
        if (!(e instanceof InvalidPhoneError || e instanceof NotAMobilePhoneError)) {
            throw e;
        }
    }

    let changedPrefix = null;

    // verifico la validità del prefisso internazionale, se ce n'è uno
    // viene ritenuto da modificare un prefisso internazione che inizia con 2 o finisce con 7
    // la modifica viene fatta solo per numeri di telefono trovati con lunghezza pari allo standard
    if (!response || response.isPossible === false) {
        let phoneNumberToCheckPrefix = text;
        if (response) {
            phoneNumberToCheckPrefix = response.correctValue;
        }

        if (phoneNumberToCheckPrefix.length >= STANDARD_PHONE_NUMBER_LENGTH) {
            if (phoneNumberToCheckPrefix.substr(0, 2) !== DEFAULT_COUNTRY_CALLING_CODE) {
                if (phoneNumberToCheckPrefix.substr(0, 2) !== DEFAULT_COUNTRY_CALLING_CODE &&
                    (
                        phoneNumberToCheckPrefix.substr(0, 1) === DEFAULT_COUNTRY_CALLING_CODE.substr(0, 1) ||
                        phoneNumberToCheckPrefix.substr(1, 1) === DEFAULT_COUNTRY_CALLING_CODE.substr(1, 1)
                    )) {

                    const oldPrefix = phoneNumberToCheckPrefix.substr(0, 2);
                    text = DEFAULT_COUNTRY_CALLING_CODE + phoneNumberToCheckPrefix.substr(2);
                    changedPrefix = oldPrefix;

                    try {
                        response = exports.test(text);
                        response.originalValue = originalText;
                        response.corrected = true;
                        response.corrections = response.corrections || {};
                        response.corrections.changedPrefix = true;
                        response.corrections.oldPrefix = oldPrefix;


                        if (response.isValid === true) {
                            return response;
                        }
                    } catch (e) {
                        if (!(e instanceof InvalidPhoneError || e instanceof NotAMobilePhoneError)) {
                            throw e;
                        }
                    }
                } else {
                    throw new InvalidPhoneError(`Invalid international prefix ${text.substr(0, 2)}`);
                }
            }
        }
    }

    if (!response || response.isPossible) {
        // il numero è possibile, per cui prendo il valore senza prefisso internazionale 
        // calcolato dalla lib e verifico se, togliendo una cifra dalla fine o dall'inizio 
        // si ottiene un numero di telefono valido

        if (!response && text.length >= STANDARD_PHONE_NUMBER_LENGTH) {
            if (text.substr(0, 2) !== DEFAULT_COUNTRY_CALLING_CODE) {
                throw new InvalidPhoneError('Not a valid phone number');
            }
        }

        let phoneNumber = text;
        let subPrefix = false;

        if (response) {
            phoneNumber = response.correctValue.substr(3);
            if (phoneNumber.length > STANDARD_PHONE_NUMBER_LENGTH - 1) {
                // se il numero di telefono ottenuto dalla lib è già della lunghezza corretta,
                // lancio un eccezione InvalidPhoneError
                throw new InvalidPhoneError('Not a valid phone number');
            }
        } else {
            // provo ad eliminare il prefisso internazionale (se esiste) dal testo base
            if (phoneNumber.length >= STANDARD_PHONE_NUMBER_LENGTH) {
                phoneNumber = phoneNumber.substr(2);
                subPrefix = true;
            }
        }

        // elimino prima l'ultimo carattere
        try {
            const result = exports.test(phoneNumber.substr(0, phoneNumber.length - 1));

            if (result.isValid === true) {
                if (!response) {
                    response = result;
                } else {
                    response.correctValue = result.correctValue;
                    response.isValid = true;
                }
                response.corrected = true;
                response.corrections = response.corrections || {};
                response.corrections.deleteEnd = phoneNumber.substr(phoneNumber.length - 1) + (response.corrections.deleteEnd || '');

                if (subPrefix) {
                    delete response.corrections.addedPrefix;
                }
                if (changedPrefix) {
                    response.corrections.changedPrefix = true;
                    response.corrections.oldPrefix = changedPrefix;
                }

                return response;
            }
        } catch (e) {
            if (!(e instanceof InvalidPhoneError || e instanceof NotAMobilePhoneError)) {
                throw e;
            }
        }

        // successivamente provo ad eliminare il primo carattere
        const result = exports.test(phoneNumber.substr(1));

        if (result.isValid === true) {
            if (!response) {
                response = result;
            } else {
                response.correctValue = result.correctValue;
                response.isValid = true;
            }
            response.corrected = true;
            response.corrections = response.corrections || {};
            response.corrections.deleteStart = response.corrections.deleteStart || '';
            response.corrections.deleteStart += phoneNumber.substr(0, 1);

            if (subPrefix) {
                delete response.corrections.addedPrefix;
            }

            if (changedPrefix) {
                response.corrections.changedPrefix = true;
                response.corrections.oldPrefix = changedPrefix;
            }

            return response;
        }
    }

    throw new InvalidPhoneError('Not a valid phone number');
}

/**
 * Verifica le modifiche base introdotte dalla lib per identificare il numero
 * 
 * @param {string} text 
 * @param {*} phoneNumber 
 * @return {PhoneNumberCorrections}
 */
function _checkNumberChanges(text, phoneNumber) {
    if (text === phoneNumber) {
        // nessuna modifica apportata
        return null;
    }

    const corrections = {};

    {
        // verifico se era almeno presente il numero completo
        const i = text.indexOf(phoneNumber);
        if (i >= 0) {
            if (i > 0) {
                corrections.deleteStart = text.substring(0, i);
            }

            if (text.length > i + phoneNumber.length) {
                corrections.deleteEnd = text.substring(i + phoneNumber.length);
            }

            return corrections;
        }
    }

    {
        // elimino il prefisso
        const pnWithoutPrefix = phoneNumber.substring(2);
        const i = text.indexOf(pnWithoutPrefix);
        if (i >= 0) {
            corrections.addedPrefix = true;

            if (i > 0) {
                corrections.deleteStart = text.substring(0, i);
            }

            if (text.length > i + phoneNumber.length) {
                corrections.deleteEnd = text.substring(i + phoneNumber.length);
            }

            return corrections;
        }
    }
}