const _ = require('lodash');
const EmptyError = require('./errors/empty-error');
const CSVParser = require("csv-parse/lib/sync");

const KEY_ID = 'id';
const KEY_PHONE_NUMBER = 'sms_phone';

module.exports = function parser(content) {
    /**
     * @type {Array}
     */
    const table = CSVParser(content, {
        columns: [KEY_ID, KEY_PHONE_NUMBER],
        skip_empty_lines: true
    });

    if (table.lenght === 0) {
        throw new EmptyError('Table is empty');
    }

    const firstItem = table[0];
    const firstItemKeys = Reflect.ownKeys(firstItem);
    
    if (firstItemKeys.length !== 2 || !firstItemKeys.includes(KEY_ID) || !firstItemKeys.includes(KEY_PHONE_NUMBER)) {
        throw new InvalidFileError('Table doesn\'t include required columns');
    }

    return table.slice(1);
}