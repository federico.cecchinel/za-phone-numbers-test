class InvalidPhoneError extends Error
{
    constructor(message) {
        super(message || "Invalid phone number");
    }
}

module.exports = InvalidPhoneError;