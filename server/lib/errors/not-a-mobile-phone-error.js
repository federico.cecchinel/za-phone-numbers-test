class InvalidPhoneError extends Error
{
    constructor(message) {
        super(message || "Not a mobile phone number");
    }
}

module.exports = InvalidPhoneError;