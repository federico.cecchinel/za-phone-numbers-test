class EmptyError extends Error
{
    constructor(message) {
        super(message || 'Value is empty');
    }
}

module.exports = EmptyError;