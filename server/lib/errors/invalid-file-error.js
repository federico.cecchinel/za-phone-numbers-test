class InvalidFileError extends Error
{
    constructor(message) {
        super(message || 'Invalid file');
    }
}

module.exports = InvalidFileError;