const _ = require('lodash');
const PhoneNumbers = require('../../src/database/collections/phone-numbers');

module.exports = async function findPhoneNumbers(search) {
    let query = {};

    if (search) {
        const regexp = new RegExp(search, "i");

        query = {
            $or: [
                { entry_id: regexp },
                { original_value: regexp },
                { phone_number: regexp }
            ]
        }
    }

    return {
        stats: await PhoneNumbers.getStats(query),
        items: await findAll(query)
    };
}

async function findAll(query = {}) {
    /**
     * @type {Array<import('../../src/database/models/phone-number').PhoneNumberLiteral>}
     */

    const items = await PhoneNumbers.find(query);

    return _.map(
        items,
        (item) => {
            const entry = {
                id: item.entry_id,
                original_value: item.original_value,
                is_valid: item.is_valid
            };

            if (entry.is_valid) {
                entry.phone_number = item.phone_number;
                entry.corrected = item.corrected || false;
                entry.corrections = item.corrections || null;
            } else {
                entry.error = item.error || 'Unknown error';
            }
            return entry;
        });
}