const testSingle = require('./test-single');
const { KEY_ID, KEY_SMS_PHONE } = require('../../globals');

module.exports = async function testMulti(table) {
    return new Promise(resolve => {
        const response = {
            stats: {
                successful: 0,
                corrected: 0,
                failure: 0
            },
            items: []
        };
    
        Promise.all(
            table.map(
                (row) => {
                    return new Promise(async (resolve) => {
                        const entry = {
                            id: row[KEY_ID],
                            original_value: row[KEY_SMS_PHONE],
                            is_valid: false
                        };
    
                        try {
                            const result = await testSingle(row[KEY_ID], row[KEY_SMS_PHONE]);
    
                            if (result) {
                                if (result.is_valid === true) {
                                    if (result.corrected === false) {
                                        response.stats.successful++;
                                    } else {
                                        response.stats.corrected++;
                                    }
        
                                    entry.phone_number = result.phone_number;
                                    entry.is_valid = result.is_valid === true;
                                    if (entry.is_valid === true) {
                                        entry.corrected = result.corrected;
                                        if ('corrections' in result) {
                                            entry.corrections = result.corrections;
                                        }
                                    }
                                } else {
                                    if ('error' in result) {
                                        entry.error = result.error;
                                    } else {
                                        entry.error = 'Unknown error';
                                    }
    
                                    response.stats.failure++;
                                }
                            } else {
                                response.stats.failure++;
                                entry.error = 'Unknown error';
                            }
                        } catch {
                            entry.error = 'Unknown error';
                            response.stats.failure++;
                        }
    
                        resolve(entry);
                    });
                }
            )
        ).then((values => {
            response.items = values;
    
            resolve(response);
        }));
    });
}