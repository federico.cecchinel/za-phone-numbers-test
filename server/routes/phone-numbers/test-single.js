const PhoneNumbers = require('../../src/database/collections/phone-numbers');
const { testAndResolve } = require('../../lib/phone-numbers-tester');
const NotAMobilePhoneError = require('../../lib/errors/invalid-phone-number-error');
const InvalidPhoneError = require('../../lib/errors/not-a-mobile-phone-error');
const { ParseError } = require('libphonenumber-js');

module.exports = function testSingle(id, text) {
    return new Promise(async (resolve, reject) => {
        const response = {
            id,
            original_value: text,
            is_valid: false
        }
        try {
            if (!id) {
                throw new Error('Missing request id');
            }
            
            /**
             * @type {import('../../lib/phone-numbers-tester').PhoneNumberTestResponse}
             */
            const result = testAndResolve(text);

            await saveEntry(id, text, result);

            response.phone_number = result.correctValue;
            response.is_valid = result.isValid;
            response.corrected = result.corrected || false;
            response.corrections = result.corrections || null;

            resolve(response);
        } catch (e) {
            if (e instanceof ParseError || e instanceof NotAMobilePhoneError || e instanceof InvalidPhoneError) {
                await saveEntry(id, text, {
                    error: e.message
                });

                response.is_valid = false;
                response.error = e.message;

                resolve(response);
                return;
            }

            reject(e);
        }
    });
}

/**
 * 
 * @param {string} id 
 * @param {string} originalValue 
 * @param {import('../../lib/phone-numbers-tester').PhoneNumberTestResponse} result 
 */
async function saveEntry(id, originalValue, result = null) {
    /**
     * @type {import('../../src/database/models/phone-number').PhoneNumberLiteral}
     */
    const entry = {
        entry_id: id,
        original_value: originalValue,
        is_valid: false
    };

    if (result) {
        if ('correctValue' in result) entry.phone_number = result.correctValue;
        if ('isValid' in result) entry.is_valid = result.isValid === true;
        if ('corrected' in result) entry.corrected = result.corrected === true;
        
        if (entry.corrected === true && 'corrections' in result) {
            entry.corrections = result.corrections;
        }

        if('error' in result) {
            entry.error = result.error
        }
    }

    await PhoneNumbers.save(entry);
}