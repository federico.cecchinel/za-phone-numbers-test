const express = require('express');
const router = express.Router();
const fs = require('fs');

const EmptyError = require('../lib/errors/empty-error');
const InvalidFileError = require('../lib/errors/invalid-file-error');
const phoneNumberCSVParser = require('../lib/phone-numbers-csv-parser');
const { KEY_ID, KEY_SMS_PHONE, KEY_UPLOADED_FILE, REQUIRED_UPLOADED_FILE_MIME_TYPE } = require('../globals');

const findPhoneNumbers = require('./phone-numbers/find');
const testSinglePhoneNumber = require('./phone-numbers/test-single');
const testMultiPhoneNumber = require('./phone-numbers/test-multi');

/* GET users listing. */
router.get('/api/phone-numbers', async function (req, res) {
    const response = {
        success: false
    };

    const search = req.query.search || '';

    try {
        const result = await findPhoneNumbers(search);
        if (result) {
            response.success = true;

            delete result.success;

            Object.assign(response, result);
        } else {
            response.error = 'Cannot retrieve items from BD';
        }
    } catch (e) {
        response.error = e.message;
    }

    res.json(response);
});

router.post('/api/phone-numbers/test', async function (req, res) {
    const id = req.body[KEY_ID];
    const text = req.body[KEY_SMS_PHONE];

    const response = {
        success: false,
        id
    };

    try {
        const result = await testSinglePhoneNumber(id, text);

        if (result) {
            response.success = true;

            delete result.success;
            delete result.id;

            Object.assign(response, result);
        } else {
            response.error = 'Invalid phone number';
        }
    } catch (e) {
        response.error = e.message;
    }

    res.json(response);
});

router.post('/api/phone-numbers/test-csv-file', async function (req, res) {
    let statusCode = 200;

    const response = {
        success: false
    };

    if (!req.files || !req.files[KEY_UPLOADED_FILE]) {
        response.error = 'No file uploaded';
        statusCode = 400;
    } else {
        const file = req.files[KEY_UPLOADED_FILE];

        if (file.mimetype !== REQUIRED_UPLOADED_FILE_MIME_TYPE) {
            response.error = 'File uploaded is not in CSV format';
            statusCode = 400;
        } else {
            try {
                let content = '';
                if (file.data) {
                    content = file.data.toString('utf8');
                } else if (file.tempFilePath) {
                    content = fs.readFileSync(file.tempFilePath);
                }

                if (!content) {
                    throw new Error('Cannot read content of file');
                }
                const table = phoneNumberCSVParser(content);

                const result = await testMultiPhoneNumber(table);
                if (!result) {
                    throw new Error('Cannot retrieve items from BD');
                }
                
                response.success = true;
                delete result.success;
                Object.assign(response, result);
            } catch (e) {
                response.error = e.message;
                
                if (e instanceof EmptyError || e instanceof InvalidFileError) {
                    statusCode = 400;
                }
            }
        }
    }

    res.json(response).status(statusCode);
});

module.exports = router;
