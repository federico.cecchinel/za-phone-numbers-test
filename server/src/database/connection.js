const _ = require('lodash');
const mongoose = require('mongoose');

const PhoneNumber = require('./models/phone-number');

exports.connect = function(connectionString) {
    return new Promise((resolve, reject) => {
        if (!_.isString(connectionString) || _.isEmpty(connectionString)) {
            reject(new Error('Database connection string invalid'));
            return;
        }

        try {
            mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});

            resolve();
        } catch (e) {
            console.error('[DATABASE] cannot connect to database', e);
            reject(e);
        }
    });
}

exports.getConnection = function() {
    return mongoose.connection;
}

exports.clearDatabase = function() {
    return Promise.all(
        [
            PhoneNumber
        ]
        .map(m => new Promise((resolve, reject) => {
            m.deleteMany({}, function (err) {
                if (err) reject(err)
                else resolve();
            });
        }))
    );
}