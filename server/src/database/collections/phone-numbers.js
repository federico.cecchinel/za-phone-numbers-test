const _ = require('lodash');
const PhoneNumberModel = require('../models/phone-number');

/**
 * 
 * @param {PhoneNumberModel|import('../models/phone-number').PhoneNumberLiteral} entry 
 */
exports.save = async function (entry) {
    if (_.isPlainObject(entry)) {
        entry = new PhoneNumberModel(entry);
    }

    if (!(entry instanceof PhoneNumberModel)) {
        reject(new Error('Cannot save this object on DB'));
        return;
    }

    entry = await entry.save();
    return true;
}

/**
 * 
 * @param {*} query 
 * @returns {Promise<import('../models/phone-number').PhoneNumberLiteral>}
 */
exports.find = async function (query = {}) {
    return new Promise((resolve, reject) => {
        PhoneNumberModel.find(query).lean().sort({created: -1}).exec((err, items) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(items || []);
        })
    })
}

/**
 * @returns {Promise<{{successful?: number, corrected?: number, failure?: number}}>}
 */
exports.getStats = async function (baseQuery = {}) {
    const stats = {
        successful: 0,
        corrected: 0,
        failure: 0
    };

    const successfullQuery = Object.assign({}, baseQuery, { is_valid: true, corrected: false });
    const correctedQuery = Object.assign({}, baseQuery, { is_valid: true, corrected: true });
    const failureQuery = Object.assign({}, baseQuery, { is_valid: false });

    return Promise.all(
        [
            successfullQuery,
            correctedQuery,
            failureQuery
        ].map(
            function (query) {
                return new Promise((resolve, reject) => {
                    PhoneNumberModel.countDocuments(query, (err, count) => {
                        if (err) {
                            console.error(err);
                            resolve(0);
                            return;
                        }
                        resolve(count);
                    })
                })
            }
        )
    ).then((values) => {
        stats.successful = values[0];
        stats.corrected = values[1];
        stats.failure = values[2];

        return Promise.resolve(stats);
    });
}

exports.toJSON = function (entry) {
    
}