const mongoose = require('mongoose');
const { Schema } = mongoose;

/**
 * @typedef {object} PhoneNumberLiteral
 * @property {string} [entry_id]
 * @property {string} [original_value]
 * @property {string} [phone_number]
 * @property {boolean} [is_valid]
 * @property {boolean} [corrected]
 * @property {PhoneNumberCorrections} [corrections]
 * @property {string} [error]
 * @property {Date} [created]
 */

const phoneNumberSchema = new Schema({
    entry_id: String,
    original_value: String,
    phone_number: {type: String, default: ''},
    is_valid: {type: Boolean, default: false},
    corrected: {type: Boolean, default: false},
    corrections: Object,
    error: {type: String, required: false},
    created: { type: Date, required: false, default: () => Date.now() }
});

phoneNumberSchema.index({entry_id: 1, original_value: 1, phone_number: 1});
phoneNumberSchema.index({created: 1});

module.exports = mongoose.model('PhoneNumber', phoneNumberSchema, 'phone_numbers');