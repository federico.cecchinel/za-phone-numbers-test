// npm modules imports
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const dotenv = require('dotenv');
const { STATUS_CODES } = require('http');

// project imports
const DBConnection = require('./src/database/connection');
const serviceApiRoutes = require('./routes/phone-numbers-api');

dotenv.config();

// main

DBConnection.connect(process.env.DATABASE);

const app = express();

// base middleware
app.use(cors());
app.use(cookieParser());

// ... for parsing application/json
app.use(bodyParser.json());

// ... for parsing application/xwww-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// ... for parsing multipart/form-data
app.use(fileUpload({
    limit: { fileSize: 2 * 1024 * 1024 },
}));
app.use(express.static('public'));



// api middleware
app.use(serviceApiRoutes);

// 404 error endpoint
app.use(function (req, res, next) {
    res.status(404).send('Unable to find the requested resource!');
});

// 500 error endpoint (uncaught exceptions)
app.use(function (err, req, res, next) {
    let error = err instanceof APIError ? err : new InternalServerError();
    if (process.env.NODE_ENV !== 'production') {
        console.error('Unhandled server error...');
        console.error(err);
    }

    res
        .status(error.status || 500)
        .json({
            code: error.code || 500,
            message: error.message || STATUS_CODES[error.status],
        });
});

module.exports = app;