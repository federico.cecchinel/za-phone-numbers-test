const location = document.location;

export const API_PORT = 3000;
export const API_BASE_URL = `${location.protocol}//${location.hostname}:${API_PORT}`;

export const API_PHONE_NUMBER_BASE_PATH = '/api/phone-numbers';
export const API_PHONE_NUMBER_FIND = `${API_PHONE_NUMBER_BASE_PATH}`;
export const API_PHONE_NUMBER_TEST = `${API_PHONE_NUMBER_BASE_PATH}/test`;
export const API_PHONE_NUMBER_TEST_CSV_FILE = `${API_PHONE_NUMBER_BASE_PATH}/test-csv-file`;

export const FORM_KEY_ID = 'id';
export const FORM_KEY_PHONE_NUMBER = 'sms_phone';

export const VUE_GLOBAL_EVENT_PHONE_LIST_UPDATED = 'phone-numbers-list-updated';