import _ from 'lodash';

/**
 * @typedef {object} PhoneNumberTestResultItem
 * @property {string} id
 * @property {string} original_value
 * @property {string} [phone_number]
 * @property {boolean} is_valid
 * @property {boolean} [corrected]
 * @property {*} [corrections]
 */

export default class PhoneNumber {
    /**
     * 
     * @param {PhoneNumberTestResultItem} [item] 
     */
    constructor(item = null) {
        this.id = null;
        this.originalValue = null;
        this.phoneNumber = null;
        this.isValid = false;
        this.isCorrected = false;
        this.corrections = null;
        this.error = null;

        this.setInfo(item);
    }

    isSuccesful() {
        return this.isValid === true && this.isCorrected === false;
    }
    hasBeenCorrected() {
        return this.isValid === true && this.isCorrected === true;
    }
    isFailed() {
        return this.isValid === false;
    }

    /**
     * 
     * @param {PhoneNumberTestResultItem} [item] 
     */
    setInfo(item = null) {
        if (PhoneNumber.isValidTestResultItem(item)) {
            this.id = item.id.toString();
            this.originalValue = item.original_value.toString();
            this.isValid = item.is_valid === true;

            if ('phone_number' in item && _.isString(item.phone_number)) {
                this.phoneNumber = item.phone_number.toString();
            }

            if ('corrected' in item) {
                this.isCorrected = item.corrected === true;
            }

            if ('corrections' in item && _.isPlainObject(item.corrections)) {
                this.corrections = item.corrections;
            }

            if ('error' in item && _.isString(item.error)) {
                this.error = item.error;
            }

            if (this.isValid === false && !this.error) {
                this.error = 'Unknown error';
            }
        }
    }

    static isValidTestResultItem(item) {
        if (_.isPlainObject(item) === false) {
            throw new TypeError('Invalid object provided');
        }

        if (!('id' in item) || typeof item.id !== 'string' || item.id.length === 0) {
            throw new Error('Missing required \'id\' field on provided object');
        }

        if (!('original_value' in item) || typeof item.original_value !== 'string' || item.original_value.length === 0) {
            throw new Error('Missing required \'original_value\' field on provided object');
        }

        if (!('is_valid' in item) || typeof item.is_valid !== 'boolean') {
            throw new Error('Missing required \'boolean\' field on provided object');
        }

        return true;
    }

    match(filter) {
        if (this.id.indexOf(filter) >= 0) return true;
        else if (this.phoneNumber && this.phoneNumber.indexOf(filter) >= 0) return true;
        else if (this.originalValue.indexOf(filter) >= 0) return true;
        return false;
    }

    getCorrectionsText() {
        if (this.isCorrected && this.corrections) {
            const keys = Reflect.ownKeys(this.corrections);
            let text = [];
            for (const key of keys) {
                switch (key) {
                    case 'addedPrefix': {
                        text.push('Added international prefix');
                        break;
                    }
                    case 'changedPrefix': {
                        text.push(`Changed international prefix from ${this.corrections.oldPrefix} to '+27'`);
                        break;
                    }
                    case 'deleteStart': {
                        text.push(`Deleted '${this.corrections.deleteStart}' before the phone number value found`);
                        break;
                    }
                    case 'deleteEnd': {
                        text.push(`Deleted '${this.corrections.deleteEnd}' after the phone number value found`);
                        break;
                    }
                }
            }
            if (text.length > 0) {
                return text.join('.<br>') + '.';
            }
        }
        return 'No corrections made.';
    }

    /**
     * 
     * @param {PhoneNumberTestResultItem} item 
     */
    static parse(item) {
        return new PhoneNumber(item);
    }
}