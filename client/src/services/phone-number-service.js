import _ from 'lodash';
import HttpRequest from './http-request';
import {
    API_BASE_URL,
    API_PHONE_NUMBER_FIND,
    API_PHONE_NUMBER_TEST,
    API_PHONE_NUMBER_TEST_CSV_FILE,
    FORM_KEY_ID,
    FORM_KEY_PHONE_NUMBER
} from '../configs/config';
import ResponseError from './response-error';
import { getStatusMessage } from './response-error';
import PhoneNumber from '../models/phone-number';
import {randomString} from './util';

export default class PhoneNumberService {
    /**
     * 
     * @param {string} apiPath 
     */
    static _getURL(apiPath) {
        if (typeof apiPath !== 'string' || apiPath.length === 0) {
            throw new TypeError('Invalid api path');
        }
        if (apiPath[0] !== '/') apiPath = `/${apiPath}`;

        const url = new URL(API_BASE_URL);
        url.pathname = apiPath;
        return url;
    }

    /**
     * 
     * @param {Response} response 
     */
    static async _onResponse(response) {
        if (response instanceof Response) {
            if (response.ok) {
                const responseJson = await response.json();

                if (responseJson.success === true) {
                    delete responseJson.success;

                    if ('items' in responseJson && responseJson.items instanceof Array) {
                        responseJson.items = _.map(responseJson.items, (pN) => new PhoneNumber(pN));
                        return responseJson;
                    } else {
                        return new PhoneNumber(responseJson);
                    }
                }
                if (responseJson.error) {
                    throw new ResponseError(responseJson.error);
                }
            }
            throw new ResponseError(getStatusMessage(response.status, response.statusText || ''), response.status);
        }
    }

    /**
     * 
     * @param {Error} err 
     */
    static _onError(err) {
        console.error(err);
        if (err instanceof ResponseError) {
            throw err;
        }
        throw new ResponseError(err.message);
    }

    static _generateId() {
        return randomString(10, '0123456789');
    }

    static async find(search) {
        try {
            const url = this._getURL(API_PHONE_NUMBER_FIND);

            if (search) {
                url.searchParams.set('search', encodeURIComponent(search));
            }

            const response = await HttpRequest.get(url, null, { mode: 'cors' });
            return this._onResponse(response);
        } catch (e) {
            this._onError(e);
        }
    }

    static async test(phoneNumber) {
        try {
            if (typeof phoneNumber !== 'string' || phoneNumber.length === 0) {
                throw new ResponseError('Invalid argument passed', 400);
            }

            const url = this._getURL(API_PHONE_NUMBER_TEST);

            const data = new FormData();
            data.append(FORM_KEY_ID, this._generateId());
            data.append(FORM_KEY_PHONE_NUMBER, phoneNumber);

            const response = await HttpRequest.post(url, data, null, { mode: 'cors' });
            return this._onResponse(response);
        } catch (e) {
            this._onError(e);
        }
    }

    static async testCSVFile(file) {
        try {
            if (!(file instanceof File)) {
                throw new ResponseError('Invalid argument passed', 400);
            }

            const url = this._getURL(API_PHONE_NUMBER_TEST_CSV_FILE);

            const response = await HttpRequest.postFile(url, file, null, { mode: 'cors' });
            return this._onResponse(response);
        } catch (e) {
            this._onError(e);
        }
    }
}