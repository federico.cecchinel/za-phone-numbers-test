import _ from 'lodash';

export default class HttpRequest {
    /**
     * 
     * @param {string} method 
     * @param {string|URL} url 
     */
    constructor(method, url) {
        if ((url instanceof URL)) {
            url = new URL(url);
        }
        this.url = url;
        this.data = null;
        this.requestOptions = {
            method,
            mode: 'same-origin'
        };
    }

    /**
     * 
     * @param {*} searchParams 
     */
    setQueryParams(searchParams = {}) {
        if (_.isPlainObject(searchParams)) {
            for (const { key, value } of searchParams) {
                this.url.searchParams.append(key, value);
            }
        }
    }

    /**
     * 
     * @param {*} options 
     */
    setOptions(options = {}) {
        if (_.isPlainObject(options)) {
            delete options.method;
            _.merge(this.requestOptions, options)
        }
    }

    /**
     * @return {Promise<Response>}
     */
    doRequest() {
        /**
         * @type {RequestInit}
         */
        const requestInit = {};
        _.merge(requestInit, this.requestOptions);
        if (this.data) {
            if (['GET', 'HEAD'].indexOf(requestInit.method) < 0) {
                requestInit.body = this.data;
            }
        }

        return fetch(this.url.toString(), requestInit);
    }

    /**
     * 
     * @param {*} url 
     * @param {*} queryParams 
     * @param {*} options 
     */
    static get(url, queryParams = {}, options = {}) {
        const request = new HttpRequest('GET', url);
        request.setQueryParams(queryParams);
        request.setOptions(options);
        return request.doRequest();
    }

    /**
     * 
     * @param {string} url 
     * @param {formData|URLSearchParams} data 
     * @param {*} options 
     */
    static post(url, data, queryParams = {}, options = {}) {
        const request = new HttpRequest('POST', url);
        request.setQueryParams(queryParams);
        request.setOptions(options);
        if (data instanceof URLSearchParams || data instanceof FormData) {
            request.data = data;
        }
        return request.doRequest();
    }

    /**
     * 
     * @param {string} url 
     * @param {File} file 
     * @param {*} options 
     */
    static postFile(url, file, queryParams = {}, options = {}) {
        const formData = new FormData();
        formData.append('file', file);
        return HttpRequest.post(url, formData, queryParams, options);
    }
}