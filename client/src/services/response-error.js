export default class ResponseError extends Error {
    constructor(message, code = 500) {
        super(message);

        this.code = code;
    }
}

/**
* Return message for HTTP status code
* @param {number} status - HTTP status code
* @returns {string} Message of network operation
*/
export function getStatusMessage(status, statusText = '') {
    let message = '';
    switch (status) {
        case 200:
            message = 'All done. Request successfully executed';
            break;
        case 201:
            message = 'Data successfully created';
            break;
        case 400:
            message = 'Bad Request';
            break;
        case 401:
            message = 'Need auth';
            break;
        case 403:
            message = 'Forbidden';
            break;
        case 404:
            message = 'Not found';
            break;
        case 500:
            message = 'Internal server error';
            break;
        case 503:
            message = 'Service unavailable. Try again later';
            break;
        default:
            message = 'Something went wrong. Request failed with status: ' + (statusText ? `${status} ${statusText}` : status);
    }
    return message;
}