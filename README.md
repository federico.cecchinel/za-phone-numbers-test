# za-phone-numbers-test
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

This tool allows you to quickly check if a given phone number corresponds to a mobile phone number in South Africa.
To do this, it uses the features offered by the [`libphonenumber-js`](https://www.npmjs.com/package/libphonenumber-js) library and a simple algorithm that corrects any typing errors and extracts the possible correct phone number

> **Note:** this tool will only discover mobile phone numbers. Other types, even if correct, will be set as invalid.


## Requirements

To run the tool GUI and API server you will need to install the following packets on your server or machine:
- NodeJS (version 12+)
- MongoDB (version 3.x, 4.x)

## GUI Interface
The tool provide a simple GUI to test your numbers. It's made entirely with Vue, using the latest ES6 specification. 

### Install
Run the following commands on a shell:

```sh
cd client
npm install
```

### Run
To run the GUI client type the following command on the shell
```sh
npm run serve
```

At this point, after compilation, the console will display the following output
```sh
 DONE  Compiled successfully in 5401ms

  App running at:
  - Local:   http://localhost:8080/ 
  - Network: http://<your-ip-address>:8080/

  Note that the development build is not optimized.
  To create a production build, run npm run build.
```

Now simply access the local or network link on a browser (like Chrome or Firefox) and try the tool.

From the tool GUI it is possible to:
- verify that a text contains a valid mobile phone number
- send a CSV file containing a table of telephone numbers to be verified
- view and search for all previously tested telephone numbers

## API Server
The API Server is a nodeJS server application written with ExpressJS. In the back it uses Mongoose for the connection to MongoDB.


### Install
Run the following commands on a shell:

```bash
cd server
npm install
```

### Run
To run the NodeJS server type the following command on the shell
```bash
npm run serve
```

### Unit test
Test are run using Mocha.
To run the Unit Test use the following command:
```bash
npm run test
```

## Type definitions

### `TestResultObject`
The Test object is defined as follows:
- `id` - a request identifier
- `original_value` - the original value provided for the test
- `phone_number` - the resulted phone number
- `is_valid` - a boolean that indicates if the test has completed successfully
- `corrected` - a boolean that indicates if the phone number has been modified in order to test as valid. Corrections will be available on `corrections` property
- `corrections` - a correction object as define in [`CorrectionObject definition`](#correctionobject) paragraph below
- `error` - an optional error message if `is_valid` is set to `false`


```typescript
{
    "id" : String,
    "original_value": String,
    "phone_number" : String,
    "is_valid" : Boolean,
    "corrected" ?: Boolean,
    "corrections" ?: CorrectionObject
    "error" ?: String
}
```

### `CorrectionObject`
The Correction object is defined as follows:
- `addedPrefix` - a boolean indicating if the international prefix has been inserted
- `changedPrefix` - a boolean indicating if the international prefix has been changed. Check `oldPrefix` property to discover the old prefix provided
- `oldPrefix` - a string indicating the internation prefix set on provided text for testing
- `deleteStart` - a string containing all the characters removed from the beginning of the provided text before finding the phone number
- `deleteEnd` - a string containing all the characters removed from the end of the provided text after finding the phone number

Every property of the Correction object is **optional**

```javascript
{
    "addedPrefix" ?: Boolean,
    "changedPrefix" ?: Boolean,
    "oldPrefix" ?: Boolean,
    "deleteStart" ?: String,
    "deleteEnd" ?: String
}
```

## APIs

The server provides the following APIs:
- [`[GET] /api/phone-numbers`](#get-apiphone-numbers): find and search phone numbers
- [`[POST] /api/phone-numbers/test`](#post-apiphone-numberstest): check if a given text includes a valid mobile phone numbers
- [`[POST] /api/phone-numbers/test-csv-file`](#post-apiphone-numberstest-csv-file): parse and test a CSV file

### `[GET] /api/phone-numbers`

Through this entry point you can find all the phone numbers registered with the `test` or `test-csv-file` APIs.
The research is done querying the MongoDB to verify for [`TestResultObject`](#testresultobject) with an `"id"`, `"original_value"` or `"phone_number"` that matches the search param provided.
The list of result items is always sorted by testing time in descendant order, in other words the latest phone number tested will be seen as the first in the list.

This API accept a single query parameter:
- [optional] `search`: String - The value to search.

[EXAMPLE RESULT]
```JSON
{
    "success": true,
    "stats": {
        "successful": 1,
        "corrected": 0,
        "failure": 0
    },
    "items": [
        {
            "id": "103426266",
            "original_value": "27746041495",
            "is_valid": true,
            "phone_number": "+27746041495",
            "corrected": false,
            "corrections": null
        }
    ]
}
```

The object in response is composed as follows:
- `success`: Boolean - indicates whether the request was successful.
- `stats`: Object - an object containing the counters of the tests found. The explanation of these three state is explained in the **"Test algorithm explained"** section
  - `successful`: Number
  - `corrected`: Number
  - `failure`: Number
- `items`: A list of all the Test found on DB or a filtered one that matches your search query. Test object definition can be found on [`TestResultObject`](#testresultobject) paragraph


### `[POST] /api/phone-numbers/test`
Through this entry point you can test a text to check if it contains a valid mobile phone number.

The API require the following data:
- [required] `id`: String - an id of the request
- [required] `sms_phone`: String - the text to verify

[EXAMPLE RESULT]
```JSON
// succesful
{
    "success": true,
    "id": "103426266",
    "original_value": "27746041495",
    "is_valid": true,
    "phone_number": "+27746041495",
    "corrected": false,
    "corrections": null
}

// failure
{
    "success": true,
    "id": "103425998",
    "original_value": "1488176172",
    "is_valid": false,
    "error": "Not a mobile number"
}
```

The object in response is composed as follows:
- `success`: Boolean - indicates whether the request was successful.
- `id`: String - the id of the test, as passed in the request post parameter
- `original_value`: Boolean - the text to parse, as passed in the request post parameter
- `is_valid`: Boolean - indicates whether the text passed contains a valid phone number
- `phone_number`: String - contains the correct phone number value in international format as verified by the libphonenumber-js library
- `corrected`: Boolean - indicates if the value has been modified in order to result valid
- `corrections`: [`CorrectionObject`](#correctionobject) - The corrections object. Object definition can be found on [`CorrectionObject definition`](#correctionobject) paragraph
- `error`: String - indicates whether the request was successful.

### `[POST] /api/phone-numbers/test-csv-file`
Through this entry point you can send a CSV file on server to test multiple values at a time. The CSV file must contain a table with at least the columns `"id"` and `"sms_phone"`. 

> Note: Parsing and testing can take some time. A test of a table of 1000 values takes around 1 seconds to be fullfilled.

The API require the following data send with `multipart/form-data` encoding:
- [required] `file`: File - the csv to send to server

[EXAMPLE RESULT]
```JSON
{
    "success": true,
    "stats": {
        "successful": 459,
        "corrected": 190,
        "failure": 351
    },
    "items": [
        {
            "id": "103426266",
            "original_value": "27746041495",
            "is_valid": true,
            "phone_number": "+27746041495",
            "corrected": false,
            "corrections": null
        },
        {
            "id": "103425998",
            "original_value": "1488176172",
            "is_valid": false,
            "error": "Not a mobile number"
        }
        ...
    ]
}
```

## Test algorithm explained
The algorithm uses as base the [`libphonenumber-js`](https://www.npmjs.com/package/libphonenumber-js) npm package to verify the correctness of a given phone number, its validity and type (Mobile, Fixed line, VoIP, Premium or toll free etc.). For this project it uses the 'max' metadata settings.
Giving a sample text like `"User phone number is +27831234567"` it can also extract the phone number from the text and validate it.

The rules followed by this simple tool are these:

1. Evaluate the text directly on libphonenumber-js to determine a possible mobile number. If it doesn't find a valid or a possible mobile number, the tool continue its excecution by editing the phone number found by the lib or the text provided in case nothing was parsed.
2. First editing made is evaluate typing error in the international prefix if present. It will change the prefix only if a single digit is wrong (for examlple it can change prefix like '+26' or '+47' into '+27', because they have respectivetly the '2' and the '7' in the right position, values as '88' hovewer are considered invalid). Then the number will be evaluated again on libphonenumber-js, and if this change isn't enought, it will continue with the step 3.
3. In this step it will check if an invalid digit has been inserted at the end of the provided number, removing it. If this test again result negativetly it will continue with the last step 4.
4. Last it will check if, after the international prefix, there is a single invalid digit. Then evaluate once again the phone number without this digit. If even this test fails the number is considered invalid.

> Note: Every test made in the steps will stop if libphonenumber-js found a valid phone number (mobile or other type) it will resolve immediately.

## Errors
The test can fail with the following errors:
- `"Not a mobile phone number"` if the found phone number is not a MOBILE phone number
- `"Not a valid phone number"` if the tool cannot find a valid phone number
- `"Invalid international prefix '+XX'` if the phone number passed has an invalid internation prefix as described in step 2. of ["Test algorigm explained"](#test-algorithm-explained) section

## License
[MIT](LICENSE)
